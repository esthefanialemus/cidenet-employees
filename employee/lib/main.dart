import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class Employee {
  var name;
  var surname;
  var phone;

  Employee({this.name, this.surname, this.phone});
}

// valores
List<Employee> employeesList = [
  Employee(name: 'Will', surname: 'Mora', phone: '302 98797'),
  Employee(name: 'Sam', surname: 'Perez', phone: '301 74845'),
  Employee(name: 'Marlon', surname: 'Gutierrez', phone: '310 98797'),
  Employee(name: 'Jhon', surname: 'Segura', phone: '320 98797')
];

// Crea un formulario
Widget _buildChart() {
  return Container(
    child: Row(
      children: <Widget>[
        Container(
          child: Table(children: _buildTableColumnOne()),
          width: numRowWidth, // Corregir la primera columna
        ),
        Expanded(
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  child: Table(children: _buildTableRow()),
                  width: numRowWidth * 6,
                )))
      ],
    ),
  );
}

// Crea la primera fila de la columna

double numRowWidth = 100.0; // ancho de tabla simple
double numRowHeight = 48.0; // altura de la mesa

List<TableRow> _buildTableColumnOne() {
  List<TableRow> returnList = [];
  returnList.add(_buildSingleColumnOne(-1));
  for (int i = 0; i < 5; i++) {
    returnList.add(_buildSingleColumnOne(i));
  }
  return returnList;
}

// Crear tableRows
List<TableRow> _buildTableRow() {
  List<TableRow> returnList = [];
  returnList.add(_buildSingleRow(-1));
  for (int i = 0; i < 5; i++) {
    returnList.add(_buildSingleRow(i));
  }
  return returnList;
}

// Crea la primera columna tableRow
TableRow _buildSingleColumnOne(int index) {
  return TableRow(
      // El estilo de la primera línea agrega color de fondo
      children: [
        // Aumentar la altura de la fila
        _buildSideBox(
            index == -1 ? 'Paper Type' : "Yellow Edge Paper", index == -1),
      ]);
}

// Crea una fila de tableRow
TableRow _buildSingleRow(int index) {
  return TableRow(
      // El estilo de la primera línea agrega color de fondo
      children: [
        _buildSideBox(index == -1 ? 'peso real' : "2676.30", index == -1),
        _buildSideBox(index == -1 ? 'Cantidad' : "100.30", index == -1),
        _buildSideBox(index == -1 ? 'Peso de tara' : "100", index == -1),
        _buildSideBox(index == -1 ? 'Precio unitario (yuanes / kg)' : "11640",
            index == -1),
        _buildSideBox(index == -1 ? 'Peso en libras (kg)' : "45", index == -1),
      ]);
}

// Crea una sola tabla
Widget _buildSideBox(String title, isTitle) {
  return SizedBox(
      height: numRowHeight,
      width: numRowWidth,
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 0.33, color: Colors.red))),
          child: Text(
            title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: isTitle ? 14 : 12, color: Colors.green),
          )));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'H:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
