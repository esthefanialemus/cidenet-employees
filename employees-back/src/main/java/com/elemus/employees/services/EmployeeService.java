package com.elemus.employees.services;

import com.elemus.employees.entity.Area;
import com.elemus.employees.entity.Country;
import com.elemus.employees.entity.DocumentType;
import com.elemus.employees.entity.Employee;
import com.elemus.employees.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    public String generatedEmail (Employee employee){
        String lastName = employee.getLastName();
        String email;
        if(lastName.contains(" ")){
            lastName.replaceAll("\\s+","");
        }
        if(employee.getCountry().equals("COLOMBIA")){
            email = employee.getFirstName()+"."+lastName+"@cidenet.com.co";
            if (repository.findByMail(email).size() != 0){
                email = employee.getFirstName()+"."+lastName+"."+employee.getId()+"@cidenet.com.co";
            }
        }else{
            email = employee.getFirstName()+"."+lastName+"@cidenet.com.us";
            if (repository.findByMail(email).size() != 0){
                email = employee.getFirstName()+"."+lastName+"."+employee.getId()+"@cidenet.com.us";
            }
        }

        return email.toLowerCase();
    }

    public List<Employee> getEmployee(){
        return  repository.findAll();
    }

    public List<Employee> getByFirstName(String firstName){
        return  repository.findByFirstName(firstName);
    }

    public List<Employee> getByMiddleName(String middleName){
        return  repository.findByMiddleName(middleName);
    }

    public List<Employee> getByLastName(String lastName){
        return  repository.findByLastName(lastName);
    }

    public List<Employee> getBySecondLastName(String secondLastName){
        return  repository.findBySecondLastName(secondLastName);
    }

    public List<Employee> getByDocumentType(DocumentType documentType){
        return  repository.findByDocumentType(documentType);
    }

    public List<Employee> getByArea(Area area){
        return  repository.findByArea(area);
    }

    public List<Employee> getByCountry(Country country){
        return  repository.findByCountry(country);
    }

    public List<Employee> getByState(String state){
        return  repository.findByState(state);
    }

    public Employee getByDocument(String document){
        return  repository.getByDocument(document);
    }
    public Employee SaveEmployee(Employee employee){
        return  repository.save(employee);
    }

    public String  deleteEmployee(String document){
            repository.deleteByDocument(document);
            return  "Employee removed" +document;
    }

    public Employee updateEmployee(Employee employee){
        boolean generatedEmail = false;
        Employee oldEmployee = getByDocument(employee.getDocument());
        if(oldEmployee == null){
            return SaveEmployee(employee);
        }
        else{
            employee.setId(oldEmployee.getId());
            if(employee.getCountry() == null){
                employee.setCountry(oldEmployee.getCountry());
            }
            if(employee.getFirstName() == null){
                employee.setFirstName(oldEmployee.getFirstName());

            }else{
                generatedEmail = true;
            }

            if(employee.getMiddleName() == null){
                employee.setMiddleName(oldEmployee.getMiddleName());
            }
            if(employee.getLastName() == null){
                employee.setLastName(oldEmployee.getLastName());

            }else{
                generatedEmail = true;
            }

            if(generatedEmail){
                employee.setMail(generatedEmail(employee));
            }
            if(employee.getSecondLastName() == null){
                employee.setSecondLastName(oldEmployee.getSecondLastName());
            }
            if(employee.getSecondLastName() == null){
                employee.setSecondLastName(oldEmployee.getSecondLastName());
            }

            if(employee.getDocumentType() == null){
                employee.setDocumentType(oldEmployee.getDocumentType());
            }
            if(employee.getDocument() == null){
                employee.setDocument(oldEmployee.getDocument());
            }

            if(employee.getArea() == null){
                employee.setArea(oldEmployee.getArea());
            }
            if(employee.getState() == null){
                employee.setState(oldEmployee.getState());
            }
            if(employee.getAddmissionDate() == null){
                employee.setAddmissionDate(oldEmployee.getAddmissionDate());
            }
            employee.setCreatedDate(oldEmployee.getCreatedDate());
            employee.setUpdateDate(LocalDateTime.now());
            return SaveEmployee(employee);
        }
    }
}
