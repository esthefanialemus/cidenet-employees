package com.elemus.employees.controller;

import com.elemus.employees.entity.Area;
import com.elemus.employees.entity.Country;
import com.elemus.employees.entity.DocumentType;
import com.elemus.employees.entity.Employee;
import com.elemus.employees.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import  java.util.List;
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @PostMapping("/addEmployee")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee)
    {
        employee.setState("Activo");
        employee=service.SaveEmployee(employee);
        employee.setMail(service.generatedEmail(employee));
        return  new ResponseEntity<>(service.SaveEmployee(employee), HttpStatus.CREATED);
    }

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> findAllEmployeeList(){
        return  new ResponseEntity<>(service.getEmployee(),HttpStatus.OK);
    }

    @GetMapping("/employees/firstName/{firstName}")
    public ResponseEntity<List<Employee>> findFistNameEmployeeList(@PathVariable String firstName){
        return new ResponseEntity<>(service.getByFirstName(firstName), HttpStatus.OK);
    }

    @GetMapping("/employees/middleName/{middleName}")
    public ResponseEntity<List<Employee>> findMiddleNameEmployeeList(@PathVariable String middleName) {
        return new ResponseEntity<>(service.getByMiddleName(middleName), HttpStatus.OK);
    }

    @GetMapping("/employees/lastName/{lastName}")
    public ResponseEntity<List<Employee>> findLastNameEmployeeList(@PathVariable String lastName) {
        return new ResponseEntity<>(service.getByLastName(lastName), HttpStatus.OK);
    }

   @GetMapping("/employees/secondLastName/{secondLastName}")
    public List<Employee> findSecondLastNameEmployeeList(@PathVariable String secondLastName) {
        return service.getBySecondLastName(secondLastName);
    }

    @GetMapping("/employees/documentType/{documentType}")
    public ResponseEntity<List<Employee>> findDocumentTypeEmployeeList(@PathVariable DocumentType documentType) {
        return new ResponseEntity<>(service.getByDocumentType(documentType), HttpStatus.OK);
    }

    @GetMapping("/employees/country/{country}")
    public ResponseEntity<List<Employee>> findCountryEmployeeList(@PathVariable Country country) {
        return new ResponseEntity<>(service.getByCountry(country), HttpStatus.OK);
    }

    @GetMapping("/employees/area/{area}")
    public ResponseEntity<List<Employee>> findAreaEmployeeList(@PathVariable Area area) {
        return new ResponseEntity<>(service.getByArea(area), HttpStatus.OK);
    }

    @GetMapping("/employees/state/{state}")
    public ResponseEntity<List<Employee>> findStateEmployeeList(@PathVariable String state) {
        return new ResponseEntity<>(service.getByState(state),HttpStatus.OK);
    }

    @GetMapping("/employees/document/{document}")
    public ResponseEntity<Employee> findDocumentEmployee(@PathVariable String document) {
        return new ResponseEntity<>(service.getByDocument(document),HttpStatus.OK);
    }

    @PutMapping("/updateEmployee")
    public ResponseEntity<Employee> updateEmployee (@RequestBody Employee employee){
        return new ResponseEntity<>(service.updateEmployee(employee),HttpStatus.OK);
    }


    @Transactional
    @DeleteMapping("/delete/{document}")
    public String deleteEmployee(@PathVariable String document){
        return service.deleteEmployee(document);
    }
}
