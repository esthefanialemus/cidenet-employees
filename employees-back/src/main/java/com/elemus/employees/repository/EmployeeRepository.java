package com.elemus.employees.repository;

import com.elemus.employees.entity.Area;
import com.elemus.employees.entity.Country;
import com.elemus.employees.entity.DocumentType;
import com.elemus.employees.entity.Employee;
import  org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    void deleteByDocument (String Document);
    List<Employee> findByArea(Area area);
    List<Employee> findByMail(String mail);
    List<Employee> findByFirstName(String firstName);
    List<Employee> findByMiddleName(String middleName);
    List<Employee> findByLastName(String lastName);
    List<Employee> findBySecondLastName(String secondLastName);
    List<Employee> findByDocumentType(DocumentType documentType);
    List<Employee> findByCountry(Country country);
    List<Employee> findByState(String state);
    Employee getByDocument(String Document);
}
