package com.elemus.employees.entity;

public enum DocumentType {
    CEDULA_CIUDADANIA,
    CEDULA_EXTRANGERIA,
    PASAPORTE,
    PERMISO_ESPECIAL
}
