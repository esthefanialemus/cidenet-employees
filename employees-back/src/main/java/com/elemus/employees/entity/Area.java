package com.elemus.employees.entity;

public enum Area {
    ADMINISTRACION,
    FINANCIERA,
    COMPRAS,
    INFRASTRUCTURA,
    OPERACION,
    TALENTO_HUMANO,
    SERVICIOS_VARIOS;
}
