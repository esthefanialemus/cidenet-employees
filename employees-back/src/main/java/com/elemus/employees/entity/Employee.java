package com.elemus.employees.entity;
import com.sun.istack.NotNull;
import jdk.jfr.Timestamp;
import lombok.*;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "EMPLOYEE")
public class Employee {
    @Id
    @GeneratedValue
    @NotNull
    private Integer id;

    @NotNull
    @Length(min = 1, max = 20)
    @Pattern(regexp="^[A-Z]+(s*[A-Z]*)*[A-Z]+$")
    private String firstName;

    @Length(min = 1, max = 50)
    @Pattern(regexp="^[A-Z]+(s*[A-Z]*)*[A-Z]+$")
    private String middleName;

    @NotNull
    @Length(min = 1, max = 20)
    @Pattern(regexp="^[A-Z]+(s*[A-Z]*)*[A-Z]+$")
    private String lastName;

    @NotNull
    @Length(min = 1, max = 20)
    @Pattern(regexp="^[A-Z]+(s*[A-Z]*)*[A-Z]+$")
    private String secondLastName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Country country;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private DocumentType documentType;

    @NotNull
    @Length(min = 1, max = 20)
    @Column(nullable=false, unique=true)
    @Pattern(regexp="^[a-zA-Z0-9_-]+$")
    private String document;

    @Email
    private String mail;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Area area;
    private String state;
    private Date addmissionDate;

    private LocalDateTime createdDate = LocalDateTime.now();
    private LocalDateTime updateDate;

}
